#include "PathFinder.h"
#include <stdio.h>

#define CELL_BLOCKED 1
#define MOVEMENT_COST_NORMAL 10
#define MOVEMENT_COST_DIAGONAL 14

PathFinder::PathFinder()
{
	pathMode = INCLUDE_ALL_DIAGONALS;
	gridLoaded = false;
	this->endTiles = new std::vector<GridCell*>();
}

PathFinder::~PathFinder()
{
}

void PathFinder::setGrid(std::vector<std::vector<int>> grid)
{
	gridLoaded = false;
	clearFullList();
	clearOpenList();
	clearClosedList();
	clearEndTileList();

	gridWidth = grid.size();
	gridHeight = grid[0].size();

	//find all end tiles
	for (int y = 0; y < gridHeight; y++)
	{
		for (int x = 0; x < gridWidth; x++)
		{
			GridCell* newCell;
			if (grid[x][y] == END_CELL)
			{
					newCell = new GridCell(x, y, CELL_STATE::END);
					newCell->parent = newCell;

					this->endTiles->push_back(newCell);
			}
		}
	}

	currentEndTile = endTiles->at(0);

	int endTileCounter = 0;
	//build full node cell list
	for (int y = 0; y < gridHeight; y++)
	{
		for (int x = 0; x < gridWidth; x++)
		{
			GridCell* newCell = 0;

			if (grid[x][y] == EMPTY_CELL)
				newCell = new GridCell(x, y, CELL_STATE::EMPTY);
			else if (grid[x][y] >= 0)
				newCell = new GridCell(x, y, CELL_STATE::SOLID);
			else if (grid[x][y] == START_CELL)
			{
				newCell = new GridCell(x, y, CELL_STATE::START);
				startTile = newCell;
			}
			else if (grid[x][y] == END_CELL)
			{ //skip end as already in the list
				newCell = endTiles->at(endTileCounter);
				endTileCounter++;
			}

			newCell->calculateHValue(currentEndTile);

			fullCellList.push_back(newCell);
		}
	}

	//configure neirbours
	for each(GridCell* cell in fullCellList)
	{
		setNeighbour(cell);
	}

	gridLoaded = true;
}

void PathFinder::setNeighbour(GridCell* cell)
{
	int posx = cell->x, posy = cell->y;

	//set left
	if (posx > 0)
	{
		cell->neighbourLeft = getCellByPosition(posx - 1, posy);
	}

	//set right
	if (posx < gridWidth - 1)
	{
		cell->neighbourRight = getCellByPosition(posx + 1, posy);
	}

	//set top
	if (posy > 0)
	{
		cell->neighbourTop = getCellByPosition(posx, posy - 1);
	}

	//set bottom
	if (posy < gridHeight - 1)
	{
		cell->neighbourBottom = getCellByPosition(posx, posy + 1);
	}

	if (pathMode == INCLUDE_UNBLOCKED_DIAGONALS || pathMode == INCLUDE_ALL_DIAGONALS)
	{
		//set top left
		if ((posy > 0) && (posx > 0))
		{
			cell->neighbourTopLeft = getCellByPosition(posx - 1, posy - 1);
		}

		//set top right
		if ((posy > 0) && (posx < gridWidth - 1))
		{
			cell->neighbourTopRight = getCellByPosition(posx + 1, posy - 1);
		}

		//set bottom left
		if ((posy < gridHeight - 1) && (posx > 0))
		{
			cell->neighbourBottomLeft = getCellByPosition(posx - 1, posy + 1);
		}

		//set bottom right
		if ((posy < gridHeight - 1) && (posx < gridWidth - 1))
		{
			cell->neighbourBottomRight = getCellByPosition(posx + 1, posy + 1);
		}
	}
}

void PathFinder::addToOpenList(GridCell* neighbour)
{
	if (neighbour != 0 && neighbour->listState == NO_LIST && neighbour->state != SOLID)
	{
		openList.push_back(neighbour);
		neighbour->listState = OPEN_LIST;
	}
}

void PathFinder::updateParent(GridCell* n, GridCell* other, bool diagonal)
{
	if (n != 0 && other != 0)
	{
		if (n->listState == OPEN_LIST)
		{
			float movementCost = 0;
			if (diagonal)
				movementCost = MOVEMENT_COST_DIAGONAL;
			else
				movementCost = MOVEMENT_COST_NORMAL;

			//add node->g to MOVEMENT COST if less than other->g set other->parent to node
			if (n->getG() + movementCost < other->getG())
			{
				other->parent = n;
			}
		}
		else if (n->listState == NO_LIST && n->state != SOLID)
		{
			float movementCost = 0;
			if (diagonal)
				movementCost = other->getG() + MOVEMENT_COST_DIAGONAL;
			else
				movementCost = other->getG() + MOVEMENT_COST_NORMAL;

			n->setG(movementCost);
			n->parent = other;
		}
	}
}

bool PathFinder::inOpenList(GridCell* target)
{
	for each (GridCell* g in openList)
	{
		if (target == g)
		{
			return true;
		}
	}
	return false;
}

bool PathFinder::linkEndToOpenlist()
{
	bool linked = false;
	if (inOpenList(currentEndTile->neighbourLeft))
	{
		currentEndTile->parent = currentEndTile->neighbourLeft;
		linked = true;
	}
	else if (inOpenList(currentEndTile->neighbourRight))
	{
		currentEndTile->parent = currentEndTile->neighbourRight;
		linked = true;
	}
	else if (inOpenList(currentEndTile->neighbourTop))
	{
		currentEndTile->parent = currentEndTile->neighbourTop;
		linked = true;
	}
	else if (inOpenList(currentEndTile->neighbourBottom))
	{
		currentEndTile->parent = currentEndTile->neighbourBottom;
		linked = true;
	}
	else if (pathMode == INCLUDE_ALL_DIAGONALS)
	{
		if (inOpenList(currentEndTile->neighbourTopLeft))
		{
			currentEndTile->parent = currentEndTile->neighbourTopLeft;
			linked = true;
		}
		else if (inOpenList(currentEndTile->neighbourTopRight))
		{
			currentEndTile->parent = currentEndTile->neighbourTopRight;
			linked = true;
		}
		else if (inOpenList(currentEndTile->neighbourBottomLeft))
		{
			currentEndTile->parent = currentEndTile->neighbourBottomLeft;
			linked = true;
		}
		else if (inOpenList(currentEndTile->neighbourBottomRight))
		{
			currentEndTile->parent = currentEndTile->neighbourBottomRight;
			linked = true;
		}
	}
	else if (pathMode == INCLUDE_UNBLOCKED_DIAGONALS)
	{
		if (inOpenList(currentEndTile->neighbourTopLeft) && bottomRightUnblocked(currentEndTile->neighbourTopLeft))
		{
			currentEndTile->parent = currentEndTile->neighbourTopLeft;
			linked = true;
		}
		else if (inOpenList(currentEndTile->neighbourTopRight) && bottomLeftUnblocked(currentEndTile->neighbourTopRight))
		{
			currentEndTile->parent = currentEndTile->neighbourTopRight;
			linked = true;
		}
		else if (inOpenList(currentEndTile->neighbourBottomLeft) && topRightUnblocked(currentEndTile->neighbourBottomLeft))
		{
			currentEndTile->parent = currentEndTile->neighbourBottomLeft;
			linked = true;
		}
		else if (inOpenList(currentEndTile->neighbourBottomRight) && topLeftUnblocked(currentEndTile->neighbourBottomRight))
		{
			currentEndTile->parent = currentEndTile->neighbourBottomRight;
			linked = true;
		}
	}
	return linked;
}

bool PathFinder::topLeftUnblocked(GridCell* cell)
{
	return cell->neighbourTop != 0 && cell->neighbourLeft != 0 && (cell->neighbourTop->state != SOLID || cell->neighbourLeft->state != SOLID);
}

bool PathFinder::topRightUnblocked(GridCell* cell)
{
	return cell->neighbourTop != 0 && cell->neighbourRight != 0 && (cell->neighbourTop->state != SOLID || cell->neighbourRight->state != SOLID);
}

bool PathFinder::bottomLeftUnblocked(GridCell* cell)
{
	return cell->neighbourBottom != 0 && cell->neighbourLeft != 0 && (cell->neighbourBottom->state != SOLID || cell->neighbourLeft->state != SOLID);
}

bool PathFinder::bottomRightUnblocked(GridCell* cell)
{
	return cell->neighbourBottom != 0 && cell->neighbourRight != 0 && (cell->neighbourBottom->state != SOLID || cell->neighbourRight->state != SOLID);
}

void PathFinder::checkNode(GridCell* node, bool drawEachFrame)
{
	//remove node from open list
	removeFromOpenList(node);

	node->listState = CLOSED_LIST;
	closedList.push_back(node);

	updateParent(node->neighbourLeft, node, false);
	updateParent(node->neighbourRight, node, false);
	updateParent(node->neighbourTop, node, false);
	updateParent(node->neighbourBottom, node, false);

	bool topLeftClear = false, topRightClear = false, bottomLeftClear = false, bottomRightClear = false;
	if (pathMode == INCLUDE_ALL_DIAGONALS)
	{
		updateParent(node->neighbourTopLeft, node, true);
		updateParent(node->neighbourTopRight, node, true);
		updateParent(node->neighbourBottomLeft, node, true);
		updateParent(node->neighbourBottomRight, node, true);
	}
	else if (pathMode == INCLUDE_UNBLOCKED_DIAGONALS)
	{
		//check each diagonal isnt blocked before attempting
		//top left
		if (topLeftUnblocked(node))
		{
			topLeftClear = true;
			updateParent(node->neighbourTopLeft, node, true);
		}

		//top right
		if (topRightUnblocked(node))
		{
			topRightClear = true;
			updateParent(node->neighbourTopRight, node, true);
		}

		//bottom left
		if (bottomLeftUnblocked(node))
		{
			bottomLeftClear = true;
			updateParent(node->neighbourBottomLeft, node, true);
		}

		//bottom right
		if (bottomRightUnblocked(node))
		{
			bottomRightClear = true;
			updateParent(node->neighbourBottomRight, node, true);
		}
	}

	//add all nieghbours not in a list into open list and self to open list
	addToOpenList(node);
	addToOpenList(node->neighbourLeft);
	addToOpenList(node->neighbourRight);
	addToOpenList(node->neighbourTop);
	addToOpenList(node->neighbourBottom);

	if (pathMode == INCLUDE_ALL_DIAGONALS)
	{
		addToOpenList(node->neighbourTopLeft);
		addToOpenList(node->neighbourTopRight);
		addToOpenList(node->neighbourBottomLeft);
		addToOpenList(node->neighbourBottomRight);
	}
	else if (pathMode == INCLUDE_UNBLOCKED_DIAGONALS)
	{
		if (topLeftClear)
			addToOpenList(node->neighbourTopLeft);

		if (topRightClear)
			addToOpenList(node->neighbourTopRight);

		if (bottomLeftClear)
			addToOpenList(node->neighbourBottomLeft);

		if (bottomRightClear)
			addToOpenList(node->neighbourBottomRight);
	}

	//try link end to list if failed keep looking
	if (!linkEndToOpenlist())
	{
		//pick lowest f value from open list and repeat
		float lowestF = 999999;
		GridCell* nextCell = 0;
		for (int i = 0; i < openList.size(); i++)
		{
			if (openList[i]->state != SOLID && openList[i]->getF() < lowestF)
			{
				nextCell = openList[i];
				lowestF = nextCell->getF();
			}
		}

		if (nextCell == 0)
		{
			return; //no route found
		}
		else if (nextCell == currentEndTile)
		{
			//handleFound
			currentEndTile->parent = node;
		}
		else
		{
			if (drawEachFrame)
			{
				system("CLS");
				draw(true);
			}

			//keep looking
			checkNode(nextCell, drawEachFrame);
		}
	}
}

void PathFinder::markCell(GridCell* cell, bool drawEachFrame)
{
	cell->state = ROUTE;
	if (cell->parent != 0 && cell->parent != cell)
	{
		if (drawEachFrame)
		{
			system("CLS");
			draw();
		}

		markCell(cell->parent, drawEachFrame);
	}
}

bool PathFinder::markRoute(bool drawEachFrame, bool drawRouteEachFrame)
{
	bool found = false;
	if (gridLoaded)
	{
		//trace route
		checkNode(startTile, drawEachFrame);

		//check if path found (if end parent not changed no path was found)
		if (currentEndTile->parent != currentEndTile)
		{
			//mark cells along route
			markCell(currentEndTile, drawRouteEachFrame);
			found = true;
		}
	}
	else
	{
		system("CLS");
		printf("Error: Cannot mark route - Grid not loaded!!");
		system("pause");
	}
	return found;
}

bool PathFinder::markAllRoutes(bool drawEachFrame, bool drawRouteEachFrame, bool forceCheckAllRoutes)
{
	bool result = true;
	int i = 1;
	for each (GridCell*	endTile in *endTiles)
	{
		printf("checking route to end: %d\n", i);
		currentEndTile = endTile;
		if (!markRoute(drawEachFrame, drawRouteEachFrame))
		{
			result = false;
			if (!forceCheckAllRoutes)
			{
				return false;
			}
			printf("no route found to end: %d\n", i);
		}
		else
		{
			printf("route found to end: %d\n", i);
		}
		reset();
		system("pause");
		i++;
	}
	return result;
}

void PathFinder::reset()
{
	clearOpenList();
	clearClosedList();

	for each (GridCell* cell in fullCellList)
	{
		cell->listState = NO_LIST;
		cell->setG(0);
	}

	startTile->listState = NO_LIST;
	startTile->setG(0);
}

void PathFinder::removeFromOpenList(GridCell* cell)
{
	for (int i = 0; i < openList.size(); i++)
	{
		if (openList[i] == cell)
		{
			openList.erase(openList.begin() + i);
			cell->listState = NO_LIST;
			return;
		}
	}
}

GridCell* PathFinder::getCellByPosition(int x, int y)
{
	for each(GridCell* cell in fullCellList)
	{
		if (cell != NULL && cell->x == x && cell->y == y)
		{
			return cell;
		}
	}

	if (startTile != NULL && startTile->x == x && startTile->y == y)
	{
		return startTile;
	}
	return NULL;
}

void PathFinder::draw(){
	draw(false);
}

void PathFinder::printParent(GridCell* cell)
{
	if (cell->parent == cell->neighbourTop)
	{
		printf("^^");
	}
	else if (cell->parent == cell->neighbourBottom)
	{
		printf("VV");
	}
	else if (cell->parent == cell->neighbourLeft)
	{
		printf("<-");
	}
	else if (cell->parent == cell->neighbourRight)
	{
		printf("->");
	}
	else if (pathMode == INCLUDE_UNBLOCKED_DIAGONALS || pathMode == INCLUDE_ALL_DIAGONALS)
	{
		if (cell->parent == cell->neighbourBottomLeft)
		{
			printf("V<");
		}
		else if (cell->parent == cell->neighbourTopLeft)
		{
			printf("^<");
		}
		else if (cell->parent == cell->neighbourTopRight)
		{
			printf("^>");
		}
		else if (cell->parent == cell->neighbourBottomRight)
		{
			printf("V>");
		}
		else
		{
			printf("**");
		}
	}
	else
	{
		printf("**");
	}
}

void PathFinder::draw(bool showClosed)
{
	if (gridLoaded)
	{
		int y = 0;
		for (int x = 0; x < fullCellList.size(); x++)
		{
			if (y != fullCellList[x]->y) //new line
			{
				y = fullCellList[x]->y;
				printf("\n");
			}

			if (fullCellList[x]->state == CELL_STATE::START)
				printf("ST");
			else if (fullCellList[x]->state == CELL_STATE::END)
				printf("EN");
			else if (fullCellList[x]->state == CELL_STATE::SOLID)
				printf("##");
			else if (showClosed && fullCellList[x]->listState == CELL_LIST_STATE::CLOSED_LIST)
				printf("CC", fullCellList[x]->getH());
			else if (fullCellList[x]->state == CELL_STATE::ROUTE)
				printParent(fullCellList[x]);
			else
				printf("__");
		}
		printf("\n");
		system("pause");
	}
	else
	{
		printf("Error: Cannot draw grid - Grid not loaded!!");
		system("pause");
	}
}

void PathFinder::setPathMode(PATH_MODE pathMode)
{
	this->pathMode = pathMode;
}