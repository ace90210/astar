#pragma once
#include <vector>

using namespace std;

class Grid
{
private:
	vector<vector<int>> grid;
	vector<vector<vector<int>>> gridList;

	void addGridOne(); // standard large
	void addGridTwo(); //standard small
	void addGridThree(); //completely blocked
	void addGridFour();  //diagaonal blocked test - top right
	void addGridFive();  //diagaonal blocked test - bottom right
	void addGridSix();  //diagaonal blocked test - top left
	void addGridSeven();  //diagaonal blocked test - bottom left
	void addGridEight();  //diagaonal blocked test - bottom left with slight gap
	void addGridNine(); // one completely blocked and one possible
public:
	Grid();
	Grid(int selectTestGrid);
	~Grid();

	void drawGrid();

	void setGridValue(int x, int y, int value);
	int getGridValue(int x, int y);
	int getWidth();
	int getHieght();

	vector<vector<int>> getGrid();
};