#pragma once
#include <math.h>

enum CELL_STATE { EMPTY, SOLID, START, END, ROUTE };
enum CELL_LIST_STATE { OPEN_LIST, CLOSED_LIST, NO_LIST };

class GridCell
{
private:
	float h; //H HEURISTIC
	float g; //G = MOVEMENT COST

	void initialise()
	{
		g = 0;
		h = 0;
		listState = NO_LIST;
		neighbourLeft = 0;
		neighbourRight = 0;
		neighbourTop = 0;
		neighbourBottom = 0;
		neighbourTopLeft = 0;
		neighbourTopRight = 0;
		neighbourBottomLeft = 0;
		neighbourBottomRight = 0;
	}

public:
	int x, y;
	CELL_STATE state;
	CELL_LIST_STATE listState;

	GridCell *parent, *neighbourLeft, *neighbourRight, *neighbourTop, *neighbourBottom, *neighbourTopLeft, *neighbourTopRight, *neighbourBottomLeft, *neighbourBottomRight;


	GridCell() : parent(0), state(EMPTY) { initialise(); }

	GridCell(int _x, int _y, GridCell *_parent = 0) {
		x = _x;
		y = _y;
		parent = _parent;
		state = EMPTY;
		initialise();
	}

	GridCell(int _x, int _y, CELL_STATE _state) {
		x = _x;
		y = _y;
		parent = 0;
		state = _state;
		initialise();
	}

	float getH() { return h; }

	float getF() { return g + h; }

	float getG() { return g; }
	void setG(float newG) { g = newG; }

	void calculateHValue(GridCell *endCell)
	{
		float x = (float)(fabsf((float)(this->x - endCell->x)));
		float y = (float)(fabsf((float)(this->y - endCell->y)));

		h = x + y;
	}
};
