#include "Grid.h"

#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <iostream>
#include "PathFinder.h"

#define WALL 1

Grid::Grid()
{
	addGridOne();
	addGridTwo();
	addGridThree();
	addGridFour();
	addGridFive();
	addGridSix();
	addGridSeven();
	addGridEight();
	addGridNine();
	grid = gridList[0];
}

Grid::Grid(int selectTestGrid)
{
	selectTestGrid--;
	addGridOne();
	addGridTwo();
	addGridThree();
	addGridFour();
	addGridFive();
	addGridSix();
	addGridSeven();
	addGridEight();
	addGridNine();

	if (selectTestGrid < 0 || selectTestGrid >= gridList.size())
	{
		grid = gridList[0];
	}
	else
	{
		grid = gridList[selectTestGrid];
	}
}

Grid::~Grid()
{
}

void Grid::addGridOne()
{
	vector<vector<int>> newGrid = vector< vector< int > >(15, vector<int>(10, CELL_TYPE::EMPTY_CELL));

	//start and end
	newGrid[1][5] = CELL_TYPE::START_CELL;
	newGrid[8][6] = CELL_TYPE::END_CELL;
	newGrid[8][9] = CELL_TYPE::END_CELL;
	newGrid[12][7] = CELL_TYPE::END_CELL;

	newGrid[4][1] = WALL;
	newGrid[5][1] = WALL;
	newGrid[5][2] = WALL;
	newGrid[5][3] = WALL;
	newGrid[5][4] = WALL;
	newGrid[5][5] = WALL;
	newGrid[5][6] = WALL;
	newGrid[5][7] = WALL;
	newGrid[5][8] = WALL;
	newGrid[5][9] = WALL;

	this->gridList.push_back(newGrid);
}

void Grid::addGridTwo()
{
	vector<vector<int>> newGrid = vector< vector< int > >(7, vector<int>(7, CELL_TYPE::EMPTY_CELL));

	//start and end
	newGrid[1][2] = CELL_TYPE::START_CELL;
	newGrid[4][5] = CELL_TYPE::END_CELL;

	newGrid[3][2] = WALL;
	newGrid[3][4] = WALL;
	newGrid[3][5] = WALL;

	this->gridList.push_back(newGrid);
}

void Grid::addGridThree()
{
	vector<vector<int>> newGrid = vector< vector< int > >(15, vector<int>(10, CELL_TYPE::EMPTY_CELL));

	//start and end
	newGrid[1][5] = CELL_TYPE::START_CELL;
	newGrid[8][6] = CELL_TYPE::END_CELL;
	newGrid[13][7] = CELL_TYPE::END_CELL;

	newGrid[5][0] = WALL;
	newGrid[5][1] = WALL;
	newGrid[5][2] = WALL;
	newGrid[5][3] = WALL;
	newGrid[5][4] = WALL;
	newGrid[5][5] = WALL;
	newGrid[5][6] = WALL;
	newGrid[5][7] = WALL;
	newGrid[5][8] = WALL;
	newGrid[5][9] = WALL;

	this->gridList.push_back(newGrid);
}

void Grid::addGridFour() //diagaonal blocked test - top right
{
	vector<vector<int>> newGrid = vector< vector< int > >(15, vector<int>(10, CELL_TYPE::EMPTY_CELL));

	//start and end
	newGrid[1][5] = CELL_TYPE::START_CELL;
	newGrid[8][6] = CELL_TYPE::END_CELL;

	newGrid[4][0] = WALL;
	newGrid[5][1] = WALL;
	newGrid[5][2] = WALL;
	newGrid[5][3] = WALL;
	newGrid[5][4] = WALL;
	newGrid[5][5] = WALL;
	newGrid[5][6] = WALL;
	newGrid[5][7] = WALL;
	newGrid[5][8] = WALL;
	newGrid[5][9] = WALL;

	this->gridList.push_back(newGrid);
}

void Grid::addGridFive()  //diagaonal blocked test - bottom right
{
	vector<vector<int>> newGrid = vector< vector< int > >(15, vector<int>(10, CELL_TYPE::EMPTY_CELL));

	//start and end
	newGrid[1][5] = CELL_TYPE::START_CELL;
	newGrid[8][6] = CELL_TYPE::END_CELL;

	newGrid[5][0] = WALL;
	newGrid[5][1] = WALL;
	newGrid[5][2] = WALL;
	newGrid[5][3] = WALL;
	newGrid[5][4] = WALL;
	newGrid[5][5] = WALL;
	newGrid[5][6] = WALL;
	newGrid[5][7] = WALL;
	newGrid[5][8] = WALL;
	newGrid[4][9] = WALL;

	this->gridList.push_back(newGrid);
}

void Grid::addGridSix()  //diagaonal blocked test - top left
{
	vector<vector<int>> newGrid = vector< vector< int > >(15, vector<int>(10, CELL_TYPE::EMPTY_CELL));

	//start and end
	newGrid[8][6] = CELL_TYPE::START_CELL;
	newGrid[1][5] = CELL_TYPE::END_CELL;

	newGrid[6][0] = WALL;
	newGrid[5][1] = WALL;
	newGrid[5][2] = WALL;
	newGrid[5][3] = WALL;
	newGrid[5][4] = WALL;
	newGrid[5][5] = WALL;
	newGrid[5][6] = WALL;
	newGrid[5][7] = WALL;
	newGrid[5][8] = WALL;
	newGrid[5][9] = WALL;

	this->gridList.push_back(newGrid);
}

void Grid::addGridSeven()  //diagaonal blocked test - bottom left
{
	vector<vector<int>> newGrid = vector< vector< int > >(15, vector<int>(10, CELL_TYPE::EMPTY_CELL));

	//start and end
	newGrid[8][6] = CELL_TYPE::START_CELL;
	newGrid[1][5] = CELL_TYPE::END_CELL;

	newGrid[5][0] = WALL;
	newGrid[5][1] = WALL;
	newGrid[5][2] = WALL;
	newGrid[5][3] = WALL;
	newGrid[5][4] = WALL;
	newGrid[5][5] = WALL;
	newGrid[5][6] = WALL;
	newGrid[5][7] = WALL;
	newGrid[5][8] = WALL;
	newGrid[6][9] = WALL;

	this->gridList.push_back(newGrid);
}

void Grid::addGridEight()  //diagaonal blocked test - bottom left
{
	vector<vector<int>> newGrid = vector< vector< int > >(15, vector<int>(10, CELL_TYPE::EMPTY_CELL));

	//start and end
	newGrid[8][6] = CELL_TYPE::START_CELL;
	newGrid[1][5] = CELL_TYPE::END_CELL;

	newGrid[5][0] = WALL;
	newGrid[5][1] = WALL;
	newGrid[5][2] = WALL;
	newGrid[5][3] = WALL;
	newGrid[5][4] = WALL;
	newGrid[5][5] = WALL;
	newGrid[5][6] = WALL;
	newGrid[5][7] = WALL;
	newGrid[5][8] = WALL;
	newGrid[7][9] = WALL;

	this->gridList.push_back(newGrid);
}

void Grid::addGridNine()
{
	vector<vector<int>> newGrid = vector< vector< int > >(15, vector<int>(10, CELL_TYPE::EMPTY_CELL));

	//start and end
	newGrid[1][5] = CELL_TYPE::START_CELL;
	newGrid[3][1] = CELL_TYPE::END_CELL;
	newGrid[13][7] = CELL_TYPE::END_CELL;

	newGrid[5][0] = WALL;
	newGrid[5][1] = WALL;
	newGrid[5][2] = WALL;
	newGrid[5][3] = WALL;
	newGrid[5][4] = WALL;
	newGrid[5][5] = WALL;
	newGrid[5][6] = WALL;
	newGrid[5][7] = WALL;
	newGrid[5][8] = WALL;
	newGrid[5][9] = WALL;

	this->gridList.push_back(newGrid);
}

void Grid::drawGrid()
{
	for (int y = 0; y < grid[0].size(); y++)
	{
		for (int x = 0; x < grid.size(); x++)
		{
			int v = grid[x][y];
			if (v == CELL_TYPE::END_CELL)
				cout << "F";
			else if (v >= 0)
				cout << "#";
			else if (v == CELL_TYPE::START_CELL)
				cout << "S";
			else
				cout << "_";

		}
		cout << endl;
	}

	system("pause");
}

void Grid::setGridValue(int x, int y, int value){
	if (x < grid.size() && y < grid[0].size()){
		grid[x][y] = value;
	}
}

int Grid::getGridValue(int x, int y){
	if (x < grid.size() && y < grid[0].size()){
		return grid[x][y];
	}
	return -1;
}

int Grid::getWidth()
{
	return grid.size();
}

int Grid::getHieght()
{
	if (getWidth() > 0)
		return grid[0].size();
	return 0;
}

vector<vector<int>> Grid::getGrid()
{
	return grid;
}