#pragma once
#include "GridCell.h"
#include <vector>

enum PATH_MODE { EXCLUDE_DIAGONALS, INCLUDE_UNBLOCKED_DIAGONALS, INCLUDE_ALL_DIAGONALS };

enum CELL_TYPE { EMPTY_CELL = -1, START_CELL = -2, END_CELL = -3 };

class PathFinder
{
private:
	bool gridLoaded;
	PATH_MODE pathMode;
	int gridWidth, gridHeight;

	GridCell* startTile, *currentEndTile;
	std::vector<GridCell*> *endTiles;
	std::vector<GridCell*> fullCellList;
	std::vector<GridCell*> openList;
	std::vector<GridCell*> closedList;


	void removeFromOpenList(GridCell* cell);
	GridCell* getCellByPosition(int x, int y);

	void updateParent(GridCell* node, GridCell* other, bool diagonal);
	void addToOpenList(GridCell* neighbour);
	bool linkEndToOpenlist();
	void checkNode(GridCell* node, bool drawEachFrame);
	bool inOpenList(GridCell* target);
	//void setNeighbourGCost(GridCell* neighbour, GridCell* newParent, bool diagonal);

	void clearOpenList() { openList.clear(); }
	void clearClosedList() { closedList.clear(); }
	void clearFullList() { fullCellList.clear(); }
	void clearEndTileList() { endTiles->clear(); }

	void markCell(GridCell* cell, bool drawEachFrame);

	void reset();

	void printParent(GridCell* cell);

	void setNeighbour(GridCell* cell);

	bool topLeftUnblocked(GridCell* cell);
	bool topRightUnblocked(GridCell* cell);
	bool bottomLeftUnblocked(GridCell* cell);
	bool bottomRightUnblocked(GridCell* cell);
public:
	PathFinder();
	~PathFinder();

	void setGrid(std::vector<std::vector<int>> grid);

	bool markRoute(bool drawEachFrame, bool drawRouteEachFrame);

	bool markAllRoutes(bool drawEachFrame, bool drawRouteEachFrame, bool forceCheckAllRoutes);

	void draw(bool showClosed);
	void draw();

	void setPathMode(PATH_MODE pathMode);
};

