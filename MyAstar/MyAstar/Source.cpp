#include "Grid.h"
#include "PathFinder.h"
#include <iostream>

using namespace std;

int main(void)
{
	//debug settings
	const PATH_MODE mode = INCLUDE_ALL_DIAGONALS;
	const bool DRAW_GRID_ON_NO_ROUTE = true;
	const bool DRAW_GRID_ON_EACH_STEP = false;
	const bool DRAW_FOUND_ROUTE_STEP_BY_STEP = false;
	const bool FORCE_ALL_ROUTE_CHECK = true;


	//set mode name string based on mode
	std::string modeName;
	switch (mode){
		case EXCLUDE_DIAGONALS: modeName = "EXCLUDE DIAGONALS"; break;
		case INCLUDE_UNBLOCKED_DIAGONALS: modeName = "INCLUDE UNBLOCKED DIAGONALS"; break;
		case INCLUDE_ALL_DIAGONALS: modeName = "INCLUDE ALL DIAGONALS"; break;
	}

	for (int i = 0; i < 9; i++)
	{
		system("CLS");
		cout << "Running test: " << i + 1 << ", MODE: " << modeName.c_str() << " - draw raw grid" << endl;

		Grid* g = new Grid(i + 1);
		g->drawGrid();

		PathFinder p = PathFinder();
		p.setPathMode(mode);
		p.setGrid(g->getGrid());

		system("CLS");
		cout << "Running test: " << i + 1 << ", MODE: " << modeName.c_str() << " - draw pathfinder grid" << endl;
		p.draw();

		system("CLS");
		cout << "Running test: " << i + 1 << ", MODE: " << modeName.c_str() << " - Display Route" << endl;

		if (p.markAllRoutes(DRAW_GRID_ON_EACH_STEP, DRAW_FOUND_ROUTE_STEP_BY_STEP, FORCE_ALL_ROUTE_CHECK))
		{
			cout << "Route found!" << endl;
			cout << "Key: ** = START, ## = WALL" << endl;
			p.draw();
		}
		else
		{
			cout << "No route found!" << endl;
			cout << "Key: ST = START, EN = Finish, ## = WALL, CC = Checked Cell" << endl;
			p.draw(DRAW_GRID_ON_NO_ROUTE);
		}	
	}

	cout << "finished" << endl;
	system("pause");
	return 0;
}

